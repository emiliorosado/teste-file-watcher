package br.org.emilio.teste.file.watcher.listener;

import java.util.Set;

import org.springframework.boot.devtools.filewatch.ChangedFile;
import org.springframework.boot.devtools.filewatch.ChangedFile.Type;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;

public class MyFileChangeListener implements FileChangeListener {

	@Override
	public void onChange(Set<ChangedFiles> changeSet) {

		String mainPath = changeSet.iterator().next().getSourceDirectory().toString();

		for(ChangedFiles cfiles : changeSet) {
			for(ChangedFile cfile : cfiles.getFiles()) {
				if (cfile.getType().equals(Type.ADD)) {
					String filePath = cfile.getFile().getAbsolutePath() ;

					System.out.println("processing the file") ;

					System.out.println("type = " + cfile.getType());
					System.out.println("mainPath = " + mainPath);
					System.out.println("filePath = " + filePath);
				}
			}
		}
	}
	
}

package br.org.emilio.teste.file.watcher.config;

import java.io.File;
import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.devtools.filewatch.FileSystemWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.org.emilio.teste.file.watcher.listener.MyFileChange2Listener;
import br.org.emilio.teste.file.watcher.listener.MyFileChangeListener;

@Configuration
public class FileWatcherConfig {

	@Value("${listener.folder}")
	private String listenerFolder;
	
	@Bean
    public FileSystemWatcher fileSystemWatcher() {
		FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, Duration.ofMillis(2000L), Duration.ofMillis(1000L));
        fileSystemWatcher.addSourceDirectory(new File(listenerFolder + "/teste1"));
        fileSystemWatcher.addSourceDirectory(new File(listenerFolder + "/teste3"));
        fileSystemWatcher.addListener(new MyFileChangeListener());
        fileSystemWatcher.start();
         
        return fileSystemWatcher;
	}
	
	@Bean
    public FileSystemWatcher fileSystemWatcher2() {
		FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, Duration.ofMillis(2000L), Duration.ofMillis(1000L));
        fileSystemWatcher.addSourceDirectory(new File(listenerFolder + "/teste2"));
        fileSystemWatcher.addListener(new MyFileChange2Listener());
        fileSystemWatcher.start();
         
        return fileSystemWatcher;
	}
	
}

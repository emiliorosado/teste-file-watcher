package br.org.emilio.teste.file.watcher.listener;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.devtools.filewatch.ChangedFile;
import org.springframework.boot.devtools.filewatch.ChangedFile.Type;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;

public class MyFileChange2Listener implements FileChangeListener {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MyFileChange2Listener.class);

	@Override
	public void onChange(Set<ChangedFiles> changeSet) {

		String mainPath = changeSet.iterator().next().getSourceDirectory().toString();

		for(ChangedFiles cfiles : changeSet) {
			for(ChangedFile cfile : cfiles.getFiles()) {
				if (cfile.getType().equals(Type.ADD)) {
					String filePath = cfile.getFile().getAbsolutePath() ;

					LOGGER.info("processing the file") ;

					LOGGER.info("type = " + cfile.getType());
					LOGGER.info("mainPath = " + mainPath);
					LOGGER.info("filePath = " + filePath);
				}
			}
		}
	}
	
}
